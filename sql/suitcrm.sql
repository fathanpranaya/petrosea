/*
Navicat MySQL Data Transfer

Source Server         : xampp
Source Server Version : 50532
Source Host           : localhost:3306
Source Database       : suitcrm

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-06-16 09:27:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `companies`
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `website` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_email_unique` (`email`),
  KEY `companies_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('1', 'PT SuitChildOMine', 'JL Pejaten 3 No 2A', 'www.suitchild.com', 'O uo suit child o mine', 'fb.me/suitchild', 'twitter.com/@suitchild', 'linkedin.com/profile/suitchild', 'contact@suitchild.com', 'Indonesia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `companies` VALUES ('2', 'coba', 'cobna', 'copnba', 'dhsi', 'fhdsjoi', 'fdhswoji', 'fhswjio', 'csdbj@cc.com', 'hfdeo', '2014-06-07 09:58:41', '2014-06-07 10:01:00', null);
INSERT INTO `companies` VALUES ('4', 'coba1dewdfe', 'cobnacewvc', 'copnbavcewv', 'dhsivcew', 'fhdsjoicvew', 'fdhswojcvei', 'fhswjiocew', 'csdbj@cccew.com', 'hfdeocew', '2014-06-07 10:00:08', '2014-06-07 10:00:08', null);
INSERT INTO `companies` VALUES ('6', 'XXX', 'vdewv', 'vrwv', 'vrwv', 'vrwv', 'vdewv', 'vwv', 'vwevrw@vcfewv.com', 'veve', '2014-06-07 10:01:17', '2014-06-07 10:04:00', null);
INSERT INTO `companies` VALUES ('7', 'vcwvc', 'vwev', 'vewvwe', 'vwevr', 'vewvwr', 'vrwv', 'vrwvrw', 'vwvrw@fewv.vv', 'vrewver', '2014-06-07 10:02:12', '2014-06-07 10:02:12', null);
INSERT INTO `companies` VALUES ('8', 'brb', 'bgrbrg', 'b', 'fvrvr', 'btreb', 'btrb', 'bgrb', 'bgr', 'brbrtg', '2014-06-07 10:19:59', '2014-06-07 10:19:59', null);

-- ----------------------------
-- Table structure for `contacts`
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `religion` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `division` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `phone_2` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `phone_3` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contacts_email_unique` (`email`),
  KEY `contacts_company_id_foreign` (`company_id`),
  KEY `contacts_role_id_foreign` (`role_id`),
  KEY `contacts_name_index` (`name`),
  CONSTRAINT `contacts_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `contacts_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `contact_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES ('1', 'Aldy Wirawan', '08151627153', 'aldy@gmail.com', '1', '5', 'aldy.wirawan', '@aldywirawan', 'aldi_wirawan', '1994-07-14', '0000-00-00 00:00:00', '2014-06-16 02:21:49', null, 'Katolik', 'Logistic', '0213123123', '0123123');
INSERT INTO `contacts` VALUES ('2', 'vfer', '3254325432', 'vfdevb@gefvb.vom', '1', '1', 'vrevr', 'ver', 'vberbv', '0000-00-00', '2014-06-07 10:18:50', '2014-06-07 10:18:50', null, '', '', '', '');
INSERT INTO `contacts` VALUES ('3', 'vfev', '32432', 'fvevrf@vbfrb.com', '1', '1', 'fewf', 'vfev', 'vvefv', '0000-00-00', '2014-06-07 10:19:23', '2014-06-07 10:19:23', null, '', '', '', '');
INSERT INTO `contacts` VALUES ('4', 'harits', '0981233123', 'harits@gmail.com', '1', '1', 'harits', '@harits', 'harits', '2014-06-03', '2014-06-12 07:40:33', '2014-06-12 07:40:33', null, '', '', '', '');

-- ----------------------------
-- Table structure for `contact_roles`
-- ----------------------------
DROP TABLE IF EXISTS `contact_roles`;
CREATE TABLE `contact_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_roles_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contact_roles
-- ----------------------------
INSERT INTO `contact_roles` VALUES ('1', 'Director', '0000-00-00 00:00:00', '2014-06-16 02:12:58', '2014-06-16 02:12:58');
INSERT INTO `contact_roles` VALUES ('2', 'Marketing', '0000-00-00 00:00:00', '2014-06-16 02:13:03', '2014-06-16 02:13:03');
INSERT INTO `contact_roles` VALUES ('3', 'Developer', '0000-00-00 00:00:00', '2014-06-16 02:12:52', '2014-06-16 02:12:52');
INSERT INTO `contact_roles` VALUES ('4', 'Sales', '0000-00-00 00:00:00', '2014-06-16 02:13:09', '2014-06-16 02:13:09');
INSERT INTO `contact_roles` VALUES ('5', 'Director', '2014-06-16 02:13:28', '2014-06-16 02:13:28', null);
INSERT INTO `contact_roles` VALUES ('6', 'Manager', '2014-06-16 02:13:35', '2014-06-16 02:13:35', null);
INSERT INTO `contact_roles` VALUES ('7', 'Staff', '2014-06-16 02:13:41', '2014-06-16 02:13:41', null);

-- ----------------------------
-- Table structure for `documents`
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `title` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `sales_id` int(10) unsigned NOT NULL,
  `salesperson_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_sales_id_foreign` (`sales_id`),
  KEY `documents_salesperson_id_foreign` (`salesperson_id`),
  KEY `documents_file_name_index` (`file_name`),
  CONSTRAINT `documents_salesperson_id_foreign` FOREIGN KEY (`salesperson_id`) REFERENCES `salespersons` (`id`),
  CONSTRAINT `documents_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of documents
-- ----------------------------
INSERT INTO `documents` VALUES ('1', '', 'tes', 'tes', '1', '1', '2014-06-12 02:17:56', '2014-06-12 05:32:52', '2014-06-12 05:32:52');
INSERT INTO `documents` VALUES ('2', '', 'cas', 'cas', '1', '1', '2014-06-12 02:18:49', '2014-06-12 05:32:58', '2014-06-12 05:32:58');
INSERT INTO `documents` VALUES ('3', 'berguru-biru.jpg', 'asdasd', 'asdas', '1', '1', '2014-06-12 02:32:37', '2014-06-12 02:32:37', null);
INSERT INTO `documents` VALUES ('4', 'cakpres.png', 'asd', 'sadsa', '1', '1', '2014-06-12 02:34:20', '2014-06-12 02:34:20', null);

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('01-2014_06_06_031612_create_contact_roles_table', '1');
INSERT INTO `migrations` VALUES ('02-2014_06_06_031612_create_companies_table', '1');
INSERT INTO `migrations` VALUES ('03-2014_06_06_031612_create_contacts_table', '1');
INSERT INTO `migrations` VALUES ('04-2014_06_06_031612_create_solution_categories_table', '1');
INSERT INTO `migrations` VALUES ('05-2014_06_06_031612_create_solutions_table', '1');
INSERT INTO `migrations` VALUES ('06-2014_06_06_031612_create_sales_referrals_table', '1');
INSERT INTO `migrations` VALUES ('07-2014_06_06_031612_create_stage_categories_table', '1');
INSERT INTO `migrations` VALUES ('08-2014_06_06_031612_create_stages_table', '1');
INSERT INTO `migrations` VALUES ('09-2014_06_06_031612_create_task_categories_table', '1');
INSERT INTO `migrations` VALUES ('10-2014_06_06_031612_create_salespersons_table', '1');
INSERT INTO `migrations` VALUES ('11-2014_06_06_031612_create_sales_priorities_table', '1');
INSERT INTO `migrations` VALUES ('12-2014_06_06_031612_create_sales_table', '1');
INSERT INTO `migrations` VALUES ('13-2014_06_06_031612_create_tasks_table', '1');
INSERT INTO `migrations` VALUES ('14-2014_06_06_031612_create_documents_table', '1');
INSERT INTO `migrations` VALUES ('15-2014_06_06_031612_create_sales_comments_table', '1');
INSERT INTO `migrations` VALUES ('16-2014_06_06_031612_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_06_06_072546_add_soft_delete_to_salespersons', '2');
INSERT INTO `migrations` VALUES ('2014_06_12_035855_add_column_role_to_users_table', '2');
INSERT INTO `migrations` VALUES ('2014_06_16_014017_add_column_religion_to_contacts', '3');
INSERT INTO `migrations` VALUES ('2014_06_16_021404_add_column_division_to_contacts', '4');
INSERT INTO `migrations` VALUES ('2014_06_16_021903_add_column_phone_2_3_to_contacts', '5');
INSERT INTO `migrations` VALUES ('2014_06_16_022334_add_column_amount_real_to_sales', '6');

-- ----------------------------
-- Table structure for `sales`
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `stage_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `salesperson_id` int(10) unsigned NOT NULL,
  `solution_id` int(10) unsigned NOT NULL,
  `contact_id` int(10) unsigned NOT NULL,
  `sales_referral_id` int(10) unsigned NOT NULL,
  `amount` int(11) NOT NULL,
  `expected_closed_date` date NOT NULL,
  `priority_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `amount_real` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_stage_id_foreign` (`stage_id`),
  KEY `sales_company_id_foreign` (`company_id`),
  KEY `sales_salesperson_id_foreign` (`salesperson_id`),
  KEY `sales_solution_id_foreign` (`solution_id`),
  KEY `sales_contact_id_foreign` (`contact_id`),
  KEY `sales_sales_referral_id_foreign` (`sales_referral_id`),
  KEY `sales_priority_id_foreign` (`priority_id`),
  KEY `sales_name_index` (`name`),
  KEY `sales_amount_index` (`amount`),
  CONSTRAINT `sales_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `sales_contact_id_foreign` FOREIGN KEY (`contact_id`) REFERENCES `contacts` (`id`),
  CONSTRAINT `sales_priority_id_foreign` FOREIGN KEY (`priority_id`) REFERENCES `sales_priorities` (`id`),
  CONSTRAINT `sales_salesperson_id_foreign` FOREIGN KEY (`salesperson_id`) REFERENCES `salespersons` (`id`),
  CONSTRAINT `sales_sales_referral_id_foreign` FOREIGN KEY (`sales_referral_id`) REFERENCES `sales_referrals` (`id`),
  CONSTRAINT `sales_solution_id_foreign` FOREIGN KEY (`solution_id`) REFERENCES `solutions` (`id`),
  CONSTRAINT `sales_stage_id_foreign` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES ('1', 'Pemilu', 'ya pemilu', '2', '1', '1', '3', '1', '2', '50000000', '2014-07-12', '2', '0000-00-00 00:00:00', '2014-06-12 02:50:24', null, '0');
INSERT INTO `sales` VALUES ('2', 'coba teyus', 'coba coba', '5', '1', '2', '2', '2', '2', '12000000', '2014-05-05', '2', '2014-06-07 09:33:29', '2014-06-12 02:50:13', null, '0');
INSERT INTO `sales` VALUES ('3', 'coba1', 'coba1', '1', '1', '1', '2', '1', '2', '1000000', '0000-00-00', '2', '2014-06-07 09:36:18', '2014-06-07 09:36:36', null, '0');
INSERT INTO `sales` VALUES ('4', 'coba', 'coba', '1', '1', '1', '1', '1', '1', '10000', '2014-06-19', '1', '2014-06-16 02:25:44', '2014-06-16 02:25:44', null, '20000');

-- ----------------------------
-- Table structure for `salespersons`
-- ----------------------------
DROP TABLE IF EXISTS `salespersons`;
CREATE TABLE `salespersons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `salespersons_email_unique` (`email`),
  KEY `salespersons_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of salespersons
-- ----------------------------
INSERT INTO `salespersons` VALUES ('1', 'admin', '08151515', 'admin@suitcrm.com', 'jalan gagal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `salespersons` VALUES ('2', 'sahe', '3243223', 'sahe@gmail.com', 'jalan sahe\r\n', '2014-06-07 10:20:46', '2014-06-12 03:57:27', null);
INSERT INTO `salespersons` VALUES ('3', 'vegvregre', '4124314', 'vwevrw@vcfewv.comdfew', 'vfevfe', '2014-06-07 10:21:06', '2014-06-07 10:21:06', null);
INSERT INTO `salespersons` VALUES ('8', 'Yogi', '081231231', 'yogi@gmail.com', 'jalan jalan dot com', '2014-06-12 04:46:23', '2014-06-12 04:46:23', null);

-- ----------------------------
-- Table structure for `sales_comments`
-- ----------------------------
DROP TABLE IF EXISTS `sales_comments`;
CREATE TABLE `sales_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sales_id` int(10) unsigned NOT NULL,
  `salesperson_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_comments_sales_id_foreign` (`sales_id`),
  KEY `sales_comments_salesperson_id_foreign` (`salesperson_id`),
  CONSTRAINT `sales_comments_salesperson_id_foreign` FOREIGN KEY (`salesperson_id`) REFERENCES `salespersons` (`id`),
  CONSTRAINT `sales_comments_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `sales_priorities`
-- ----------------------------
DROP TABLE IF EXISTS `sales_priorities`;
CREATE TABLE `sales_priorities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_priorities_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_priorities
-- ----------------------------
INSERT INTO `sales_priorities` VALUES ('1', 'High', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `sales_priorities` VALUES ('2', 'Medium', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `sales_priorities` VALUES ('3', 'Low', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `sales_referrals`
-- ----------------------------
DROP TABLE IF EXISTS `sales_referrals`;
CREATE TABLE `sales_referrals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_referrals_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales_referrals
-- ----------------------------
INSERT INTO `sales_referrals` VALUES ('1', 'None', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `sales_referrals` VALUES ('2', 'Website', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `sales_referrals` VALUES ('3', 'Advertisement', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `sales_referrals` VALUES ('4', 'Person', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `solutions`
-- ----------------------------
DROP TABLE IF EXISTS `solutions`;
CREATE TABLE `solutions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `solution_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `solutions_solution_category_id_foreign` (`solution_category_id`),
  KEY `solutions_name_index` (`name`),
  CONSTRAINT `solutions_solution_category_id_foreign` FOREIGN KEY (`solution_category_id`) REFERENCES `solution_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of solutions
-- ----------------------------
INSERT INTO `solutions` VALUES ('1', 'Website Application', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solutions` VALUES ('2', 'Mobile Application', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solutions` VALUES ('3', 'Computer Server', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solutions` VALUES ('4', 'Social Media', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solutions` VALUES ('5', 'Advertising', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solutions` VALUES ('6', 'Hosting', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `solution_categories`
-- ----------------------------
DROP TABLE IF EXISTS `solution_categories`;
CREATE TABLE `solution_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `solution_categories_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of solution_categories
-- ----------------------------
INSERT INTO `solution_categories` VALUES ('1', 'Product', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `solution_categories` VALUES ('2', 'Service', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `stages`
-- ----------------------------
DROP TABLE IF EXISTS `stages`;
CREATE TABLE `stages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stage_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `stage_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stages_stage_category_id_foreign` (`stage_category_id`),
  KEY `stages_stage_name_index` (`stage_name`),
  CONSTRAINT `stages_stage_category_id_foreign` FOREIGN KEY (`stage_category_id`) REFERENCES `stage_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stages
-- ----------------------------
INSERT INTO `stages` VALUES ('1', 'Follow Up', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('2', 'Non Prospect', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('3', 'Opportunity Qualification', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('4', 'Assesment', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('5', 'Solutioning', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('6', 'Confirm Solution', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('7', 'Propose Stage', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('8', 'Negotiation', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('9', 'Win or Lose', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('10', 'Decommit', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('11', 'Implementation Done', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('12', 'Deal', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stages` VALUES ('13', 'Dead', '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `stage_categories`
-- ----------------------------
DROP TABLE IF EXISTS `stage_categories`;
CREATE TABLE `stage_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stage_categories_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of stage_categories
-- ----------------------------
INSERT INTO `stage_categories` VALUES ('1', 'Lead', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stage_categories` VALUES ('2', 'Prospect', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `stage_categories` VALUES ('3', 'Deal', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for `tasks`
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `deadline` datetime NOT NULL,
  `sales_id` int(10) unsigned NOT NULL,
  `salesperson_id` int(10) unsigned NOT NULL,
  `task_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_sales_id_foreign` (`sales_id`),
  KEY `tasks_salesperson_id_foreign` (`salesperson_id`),
  KEY `tasks_task_category_id_foreign` (`task_category_id`),
  KEY `tasks_name_index` (`name`),
  KEY `tasks_deadline_index` (`deadline`),
  CONSTRAINT `tasks_salesperson_id_foreign` FOREIGN KEY (`salesperson_id`) REFERENCES `salespersons` (`id`),
  CONSTRAINT `tasks_sales_id_foreign` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`),
  CONSTRAINT `tasks_task_category_id_foreign` FOREIGN KEY (`task_category_id`) REFERENCES `task_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tasks
-- ----------------------------
INSERT INTO `tasks` VALUES ('1', 'coba', '2014-06-13 00:00:00', '1', '1', '1', '2014-06-11 07:05:49', '2014-06-11 07:05:49', null);

-- ----------------------------
-- Table structure for `task_categories`
-- ----------------------------
DROP TABLE IF EXISTS `task_categories`;
CREATE TABLE `task_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task_categories_name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of task_categories
-- ----------------------------
INSERT INTO `task_categories` VALUES ('1', 'Meeting', '2014-06-11 06:23:20', '2014-06-11 06:23:20', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salesperson_id` int(10) unsigned NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_salesperson_id_foreign` (`salesperson_id`),
  KEY `users_password_index` (`password`),
  CONSTRAINT `users_salesperson_id_foreign` FOREIGN KEY (`salesperson_id`) REFERENCES `salespersons` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('11', 'admin', '$2y$10$88R91F7T6zbOOAl00Uf6m.2JYOb7fX.rfWW2bXCU4di09fcXjJ3Wy', null, '1', 'Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `users` VALUES ('13', 'sahe', '$2y$10$VK2v9Q75p03cn6ffSmtzSe5Fu2KnSxW2fSLkELu7joJ0zLSsfCF5G', 'RevJuIsM15juRiWqIrOlYIA7kw5p98rA7LhPlpKNeMWPRb3QxZZA9rxNNrcG', '2', 'Sales', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `users` VALUES ('14', 'yogi', '$2y$10$xw4Ab0ArzRIVLrPma3vsAO00xq6SuiJgWi0gyA08gWMeP7OWFa.8y', '2qL1H6HH4UR3cX5Kyu5GwED1I8cosI27ouQXqgcGmnaYxSiiAkeX3n7fZkRE', '8', 'Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
