/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50614
Source Host           : localhost:3306
Source Database       : petrosea

Target Server Type    : MYSQL
Target Server Version : 50614
File Encoding         : 65001

Date: 2014-08-16 13:12:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `assigned_projects`
-- ----------------------------
DROP TABLE IF EXISTS `assigned_projects`;
CREATE TABLE `assigned_projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_material_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tyest` (`project_id`),
  KEY `test2` (`stock_material_id`),
  CONSTRAINT `test2` FOREIGN KEY (`stock_material_id`) REFERENCES `stock_materials` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tyest` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assigned_projects
-- ----------------------------
INSERT INTO `assigned_projects` VALUES ('2', '1', '1', '2014-08-05 09:50:33', '2014-08-05 09:50:33');
INSERT INTO `assigned_projects` VALUES ('3', '1', '1', '2014-08-05 09:50:37', '2014-08-05 09:50:37');

-- ----------------------------
-- Table structure for `buyers`
-- ----------------------------
DROP TABLE IF EXISTS `buyers`;
CREATE TABLE `buyers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `long_work` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of buyers
-- ----------------------------
INSERT INTO `buyers` VALUES ('1', 'Fathan', 'Managers', '10', '2014-08-01 04:51:03', '2014-08-01 05:43:50');
INSERT INTO `buyers` VALUES ('2', 'test', 'test', '18', '2014-08-01 06:36:27', '2014-08-01 06:36:27');

-- ----------------------------
-- Table structure for `currencies`
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES ('1', '$', 'Dollar', '2014-08-01 05:56:09', '2014-08-01 05:56:09');
INSERT INTO `currencies` VALUES ('2', 'Rp', 'Rupiah', '2014-08-01 06:37:00', '2014-08-01 06:37:00');

-- ----------------------------
-- Table structure for `delivery_orders`
-- ----------------------------
DROP TABLE IF EXISTS `delivery_orders`;
CREATE TABLE `delivery_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) unsigned DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` text,
  `confirmance_status` tinyint(4) DEFAULT NULL,
  `delivery_status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `do_po` (`purchase_order_id`),
  CONSTRAINT `do_po` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_orders` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of delivery_orders
-- ----------------------------

-- ----------------------------
-- Table structure for `incoterms`
-- ----------------------------
DROP TABLE IF EXISTS `incoterms`;
CREATE TABLE `incoterms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of incoterms
-- ----------------------------
INSERT INTO `incoterms` VALUES ('1', 'Incoterms1', 'haha hihi huhu hoho\r\n', '2014-08-01 05:46:01', '2014-08-01 05:46:07');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('16-2014_06_06_031612_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_06_12_035855_add_column_role_to_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_08_01_044702_add_time_stamps', '2');

-- ----------------------------
-- Table structure for `payment_terms`
-- ----------------------------
DROP TABLE IF EXISTS `payment_terms`;
CREATE TABLE `payment_terms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payment_terms
-- ----------------------------
INSERT INTO `payment_terms` VALUES ('3', 'test', 'test', '2014-08-02 02:14:34', '2014-08-02 02:14:34');

-- ----------------------------
-- Table structure for `pick_tickets`
-- ----------------------------
DROP TABLE IF EXISTS `pick_tickets`;
CREATE TABLE `pick_tickets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_material_id` int(11) unsigned DEFAULT NULL,
  `uom_id` int(11) unsigned DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `person_in_charge` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pt_sm` (`stock_material_id`),
  KEY `pt_uom` (`uom_id`),
  CONSTRAINT `pt_sm` FOREIGN KEY (`stock_material_id`) REFERENCES `stock_materials` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pt_uom` FOREIGN KEY (`uom_id`) REFERENCES `uoms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pick_tickets
-- ----------------------------
INSERT INTO `pick_tickets` VALUES ('1', '1', '1', '100', '2013-01-01', '2013-01-01', 'Fathanzz', '2014-08-05 08:06:14', '2014-08-05 08:12:02');
INSERT INTO `pick_tickets` VALUES ('3', '1', '1', '11', '2014-08-04', '2014-08-14', 'gue', '2014-08-14 13:46:29', '2014-08-14 13:46:29');

-- ----------------------------
-- Table structure for `projects`
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `description` longtext,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('1', 'Project1', 'Bekasi', '9', null, 'Hahahaha', '2014-08-01 05:51:05', '2014-08-01 05:51:09');

-- ----------------------------
-- Table structure for `purchase_orders`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_orders`;
CREATE TABLE `purchase_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_material_id` int(11) unsigned DEFAULT NULL,
  `vendor_id` int(11) unsigned DEFAULT NULL,
  `buyer_id` int(11) unsigned DEFAULT NULL,
  `uom_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `incoterm_id` int(11) unsigned DEFAULT NULL,
  `payment_term_id` int(11) unsigned DEFAULT NULL,
  `po_subject` varchar(255) DEFAULT NULL,
  `po_date` date DEFAULT NULL,
  `quantity_material` int(11) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `delivery_address` longtext,
  `approval_status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `po_to_uom` (`uom_id`),
  KEY `FK_po_to_sm` (`stock_material_id`),
  KEY `po_curr` (`currency_id`),
  KEY `po_i` (`incoterm_id`),
  KEY `po_payment` (`payment_term_id`),
  KEY `po_to_b` (`buyer_id`),
  KEY `po_to_v` (`vendor_id`),
  CONSTRAINT `FK_po_to_sm` FOREIGN KEY (`stock_material_id`) REFERENCES `stock_materials` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_curr` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_i` FOREIGN KEY (`incoterm_id`) REFERENCES `incoterms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_payment` FOREIGN KEY (`payment_term_id`) REFERENCES `payment_terms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_to_b` FOREIGN KEY (`buyer_id`) REFERENCES `buyers` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_to_uom` FOREIGN KEY (`uom_id`) REFERENCES `uoms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `po_to_v` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_orders
-- ----------------------------
INSERT INTO `purchase_orders` VALUES ('2', '1', '1', '1', '1', '1', '1', '3', 'zzz', '2014-12-03', '12', '2014-12-03', '12', '121212', 'JL Kecubung VI No 138 Rawalumbu Bekasi Jawa Barat Indonesia 17116\r\n\r\nTelepon 82401089\r\nHandphone 0815151515', '12', '2014-08-02 01:36:26', '2014-08-02 02:14:56');

-- ----------------------------
-- Table structure for `purchase_requests`
-- ----------------------------
DROP TABLE IF EXISTS `purchase_requests`;
CREATE TABLE `purchase_requests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `requester_id` int(11) unsigned DEFAULT NULL,
  `stock_material_id` int(11) unsigned DEFAULT NULL,
  `po_id` int(11) unsigned DEFAULT NULL,
  `uom_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `pr_subject` varchar(255) DEFAULT NULL,
  `pr_date` date DEFAULT NULL,
  `quantity_material` int(11) DEFAULT NULL,
  `required_date` date DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `person_in_charge` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pr_curr` (`currency_id`),
  KEY `pr_po` (`po_id`),
  KEY `pr_r` (`requester_id`),
  KEY `pr_sm` (`stock_material_id`),
  KEY `pr_uom` (`uom_id`),
  CONSTRAINT `pr_curr` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pr_po` FOREIGN KEY (`po_id`) REFERENCES `purchase_orders` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pr_r` FOREIGN KEY (`requester_id`) REFERENCES `requesters` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pr_sm` FOREIGN KEY (`stock_material_id`) REFERENCES `stock_materials` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pr_uom` FOREIGN KEY (`uom_id`) REFERENCES `uoms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchase_requests
-- ----------------------------
INSERT INTO `purchase_requests` VALUES ('1', '1', '1', '2', '1', '1', 'asasas', '2013-01-01', '1', '2013-01-01', '12', '12', 'Fathan', '2014-08-05 09:02:34', '2014-08-05 09:02:34');

-- ----------------------------
-- Table structure for `quotations`
-- ----------------------------
DROP TABLE IF EXISTS `quotations`;
CREATE TABLE `quotations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_material_id` int(11) unsigned DEFAULT NULL,
  `vendor_id` int(11) unsigned DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `price_quote_status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `q_sm` (`stock_material_id`),
  KEY `q_v` (`vendor_id`),
  CONSTRAINT `q_sm` FOREIGN KEY (`stock_material_id`) REFERENCES `stock_materials` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `q_v` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of quotations
-- ----------------------------
INSERT INTO `quotations` VALUES ('2', '1', '1', '100', '12', '12', '121', '2014-08-05 09:23:57', '2014-08-05 09:23:57');

-- ----------------------------
-- Table structure for `requesters`
-- ----------------------------
DROP TABLE IF EXISTS `requesters`;
CREATE TABLE `requesters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `long_work` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of requesters
-- ----------------------------
INSERT INTO `requesters` VALUES ('1', 'Fathan', 'mintaers', '1000', '2014-08-01 06:41:00', '2014-08-01 06:41:08');
INSERT INTO `requesters` VALUES ('2', 'Jokowi', 'mintaers', '10000', '2014-08-01 06:41:16', '2014-08-01 06:42:18');

-- ----------------------------
-- Table structure for `stock_materials`
-- ----------------------------
DROP TABLE IF EXISTS `stock_materials`;
CREATE TABLE `stock_materials` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uom_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `sm_uom` (`uom_id`),
  CONSTRAINT `sm_uom` FOREIGN KEY (`uom_id`) REFERENCES `uoms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stock_materials
-- ----------------------------
INSERT INTO `stock_materials` VALUES ('1', '1', 'haha', '1', 'hihi', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `uoms`
-- ----------------------------
DROP TABLE IF EXISTS `uoms`;
CREATE TABLE `uoms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uoms
-- ----------------------------
INSERT INTO `uoms` VALUES ('1', 'EA', 'Each', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `uoms` VALUES ('2', 'Lt', 'Liter', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `uoms` VALUES ('3', 'Dr', 'Drum', '0000-00-00 00:00:00', '2014-08-01 06:43:41');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_password_index` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '$2y$10$B7VYERe.rlIqK8VqKJUABucn5bYuWWYN4XIBPeOxKb/TKzGgQmw4K', 'admin@petrosea.com', 'admin', 'ekNqrTSeNKNc7WxQjJSYZXnBuBYOtBiIgWxwcLUBQghdKXQlapP0u6muLd6W', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `users` VALUES ('2', 'inventory', '$2y$10$hQrWLHOtvWETC.nb6pFu5u2HZmMOrk7V5HhdDdVGf3wB8.3bNuj3a', 'inventory@petrosea.com', 'inventory', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `users` VALUES ('3', 'procurement', '$2y$10$tejw1bTtNVA2Q7VJeol2neonUL6P7eNiQO4reeFeIxj395epfq7ES', 'procurement@petrosea.com', 'procurement', 'xWHCwXe5qAUjDTTlFVJ2j9TioR9Wr9wDBifw5oD7IDASkcfFWNtjJG43CGCE', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `users` VALUES ('4', 'warehouse', '$2y$10$.ofSGC95L7ApSYgLYB6CxOD.Cz0kyaVPSVTNr5mGlc0NnWgzL1Zj2', 'warehouse@petrosea.com', 'warehouse', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `users` VALUES ('5', 'external', '$2y$10$CTxZh.sk8gJEqzZPxYUqFesZx.itT3ozQgR8P/c3jhFVVDlT9SKha', 'external@petrosea.com', 'external', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `vendors`
-- ----------------------------
DROP TABLE IF EXISTS `vendors`;
CREATE TABLE `vendors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `area_of_expertise` varchar(255) DEFAULT NULL,
  `contract_status` tinyint(4) DEFAULT NULL,
  `performance_score` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vendors
-- ----------------------------
INSERT INTO `vendors` VALUES ('1', 'Fathan', 'informatics', '10', '10', '2014-08-01 09:47:54', '2014-08-01 09:47:54');
