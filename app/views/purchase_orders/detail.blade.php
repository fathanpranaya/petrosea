@extends('layouts.master')

@section('script')

<script>
    $('#datepick').datetimepicker({
        pickTime: false
    });
</script>

@stop

@section('content')
    <div class="well">

        <div class="row">


            <div class="col-lg-2"></div>
            
            <div class="col-lg-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Purchase Order - {{$purchase_order->id}}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tr>
                                <td>Subject</td>
                                <td>{{$purchase_order->po_subject}}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>{{$purchase_order->po_date}}</td>
                            </tr>
                            <tr>
                                <td>Stock Material</td>
                                <td>{{@$purchase_order->stockMaterial->name}}</td>
                            </tr>
                            <tr>
                                <td>Vendor</td>
                                <td>{{@$purchase_order->vendor->name}}</td>
                            </tr>
                            <tr>
                                <td>Buyer</td>
                                <td>{{@$purchase_order->buyer->name}}</td>
                            </tr>
                            <tr>
                                <td>Incoterm</td>
                                <td>{{@$purchase_order->incoterm->name}}</td>
                            </tr>
                            <tr>
                                <td>Payment Term</td>
                                <td>{{@$purchase_order->payment_term->name}}</td>
                            </tr>
                            <tr>
                                <td>Delivery Date</td>
                                <td>{{$purchase_order->delivery_date}}</td>
                            </tr>
                            <tr>
                                <td>Quantity Material</td>
                                <td>{{$purchase_order->quantity_material.' '.@$purchase_order->uom->description}}</td>
                            </tr>
                            <tr>
                                <td>Unit Price</td>
                                <td>{{@$purchase_order->currency->name.' '.$purchase_order->unit_price}}</td>
                            </tr>
                            <tr>
                                <td>Total Price</td>
                                <td><b>{{@$purchase_order->currency->name.' '.$purchase_order->unit_price*$purchase_order->quantity_material}}</b></td>
                            </tr>
                            <tr>
                                <td>Delivery Address</td>
                                <td>{{$purchase_order->delivery_address}}</td>
                            </tr>
                            <tr>
                                <td>Approval Status</td>
                                <td>{{$purchase_order->approval_status}}</td>
                            </tr>
                            
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
            </div>
             
        </div>

    </div>
@stop