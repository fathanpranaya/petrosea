@extends('layouts.master')

@section('script')

<script>

</script>

@stop

@section('content')
    <div class="well">

        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-5">
                <h1 style="text-allign:center">Edit Purchase Order</h1>
            </div>
        </div>
        </hr>

        <div class="row">
             {{Form::model($purchase_order, ['route'=>['purchase_orders.update', $purchase_order->id], 'method' => 'PUT'])}}

            <div class="col-lg-2"></div>
            
            <div class="col-lg-4">
                <div class="form-group">
                    {{Form::label('stock_material', 'Stock Material')}}
                    {{Form::select('stock_material_id', StockMaterial::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'stock_material_id'])}}
                    {{$errors->first('stock_material_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('vendor', 'Vendor')}}
                    {{Form::select('vendor_id', Vendor::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'vendor_id'])}}
                    {{$errors->first('vendor_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('buyer', 'Buyer')}}
                    {{Form::select('buyer_id', Buyer::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'buyer_id'])}}
                    {{$errors->first('buyer_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('incoterm', 'Incoterm')}}
                    {{Form::select('incoterm_id', Incoterm::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'incoterm_id'])}}
                    {{$errors->first('incoterm_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('payment_term', 'Payment Term')}}
                    {{Form::select('payment_term_id', PaymentTerm::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'payment_term_id'])}}
                    {{$errors->first('payment_term_id')}}
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('quantity_material', 'Quantity')}}
                            {{Form::text('quantity_material', null, ['class'=>'form-control', 'id'=>'qt'])}}
                            {{$errors->first('quantity_material')}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('uom', 'Unit of Measure')}}
                            {{Form::select('uom_id', Uom::lists('description', 'id'), null, ['class'=>'form-control', 'id'=>'uom_id'])}}
                            {{$errors->first('uom_id')}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('unit_price', 'Unit Price')}}
                            {{Form::text('unit_price', null, ['class'=>'form-control', 'id'=>'price'])}}
                            {{$errors->first('unit_price')}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('currency', 'Currency')}}
                            {{Form::select('currency_id', Currency::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'currency_id'])}}
                            {{$errors->first('currency_id')}}
                        </div>
                    </div>
                </div>        
            </div>
            
            <div class="col-lg-4">
                <div class="form-group">
                    {{Form::label('po_subject', 'Subject')}}
                    {{Form::text('po_subject', null, ['class'=>'form-control'])}}
                    {{$errors->first('po_subject')}}
                </div>

                <div class="form-group">
                    {{Form::label('po_date', 'Date')}}
                    <div class="input-group date" id="datepick1" data-date-format="YYYY-MM-DD">
                        {{Form::text('po_date', null, array('class'=>'form-control '))}}    
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    {{$errors->first('po_date')}}
                </div>

                <div class="form-group">                    
                    {{Form::label('delivery_date', 'Expected Closed Date')}}
                    <div class="input-group date" id="datepick2" data-date-format="YYYY-MM-DD">
                        {{Form::text('delivery_date', null, array('class'=>'form-control '))}}    
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    {{$errors->first('delivery_date')}}
                </div>

                <div class="form-group">
                    {{Form::label('delivery_address', 'Delivery Address')}}
                    {{Form::textarea('delivery_address', null, ['class'=>'form-control', 'rows'=>7])}}
                    {{$errors->first('delivery_address')}}
                </div>

                <div class="form-group">
                    {{Form::label('approval_statuss', 'Approval Status')}}
                    <br/>
                    {{Form::radio('approval_status', 1)}}
                    <span>Deal</span>
                    {{Form::radio('approval_status', 0, true)}}
                    <span>No Deal</span>
                    {{$errors->first('approval_status')}}
                </div>

                <div class="form-group">
                    {{Form::submit('Save', ['class'=>'btn btn-primary', 'style' => 'width:100%'])}}
                </div>
            </div>

            {{Form::close()}}
                
        </div>

    </div>
@stop