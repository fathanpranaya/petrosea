@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
    <a href="{{url('purchase_orders/create')}}" class="btn btn-primary form-control"><span class="glyphicon glyphicon-plus-sign"></span> New Purchase Order</a>
    <hr>
    
    <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Date</th>
                <th>Stock Material</th>
                <th>Vendor</th>
                <th>Buyer</th>
                <th>Incoterm</th>
                <th>Payment Term</th>
                <th>Menu</th>
            </tr>
        </thead>
 
 
        <tbody>
            @foreach ($purchase_orders as $purchase_order)
                <tr>
                    <td>
                        <a href="{{url('purchase_orders/'.$purchase_order->id)}}">
                            {{$purchase_order->po_subject}}
                        </a>
                    </td>        
                    <td>{{$purchase_order->po_date}}</td>
                    <td>{{@$purchase_order->stockMaterial->name}}</td>
                    <td>{{@$purchase_order->vendor->name}}</td>
                    <td>{{@$purchase_order->buyer->name}}</td>
                    <td>{{@$purchase_order->incoterm->name}}</td>
                    <td>{{@$purchase_order->paymentTerm->name}}</td>
                    <td style="text-align:center">
                        <a href="{{url('purchase_orders/'.$purchase_order->id.'/edit')}}" class="btn btn-sm btn-success">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a> 
                        <a href="{{url('purchase_orders/destroy/'.$purchase_order->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            @endforeach            
        </tbody>
    </table> </div>
    
@stop