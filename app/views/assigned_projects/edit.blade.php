@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
<h3>Assigned Project</h3>
<div class="row">
    <div class="col-lg-4">
        <div class="well">
         {{Form::model($assigned_project, ['route'=>['assigned_projects.update', $assigned_project->id], 'method' => 'PUT'])}}
            <div class="form-group">
                {{Form::label('project_id', 'Project')}}
                {{Form::select('project_id', Project::lists('name', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('project_id')}}
            </div>
            <div class="form-group">
                {{Form::label('stock_material', 'Stock Material')}}
                {{Form::select('stock_material_id', StockMaterial::lists('name', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('stock_material_id')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>

    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Project Name</th>
                    <th>Stock Material</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($assigned_projects as $assigned_project)
                    
                    <tr>
                        <td >{{@$assigned_project->project->name}}</td>
                        <td >{{@$assigned_project->stockMaterial->name}}</td>
                        <td style="text-align:center">
                            <a href="{{url('assigned_projects/'.$assigned_project->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('assigned_projects/destroy/'.$assigned_project->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop