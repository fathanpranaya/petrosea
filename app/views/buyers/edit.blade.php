@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
<h3>Edit Buyer</h3>
<div class="row">
    <div class="col-lg-4">
        <div class="well">
        {{Form::model($buyer, ['route'=>['buyers.update', $buyer->id], 'method' => 'PUT'])}}
            <div class="form-group">
                {{Form::label('name', 'Name')}}
                {{Form::text('name', null, ['class'=>'form-control', 'rows' => 2])}}
                {{$errors->first('name')}}
            </div>
            <div class="form-group">
                {{Form::label('position', 'Position')}}
                {{Form::text('position', null, ['class'=>'form-control', 'rows' => 2])}}
                {{$errors->first('position')}}
            </div>
            <div class="form-group">
                {{Form::label('long_work', 'Long Work')}}
                <div class="input-group">   
                    {{Form::text('long_work', null, ['class'=>'form-control', 'rows' => 2])}}
                    <span class="input-group-addon">Years</span>
                </div>
                {{$errors->first('long_work')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>

    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Long Work</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($buyers as $buyer)
                    
                    <tr>
                        <td >{{$buyer->name}}</td>
                        <td >{{$buyer->position}}</td>
                        <td >{{$buyer->long_work.' years'}}</td>
                        <td style="text-align:center">
                            <a href="{{url('buyers/'.$buyer->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('buyers/destroy/'.$buyer->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop