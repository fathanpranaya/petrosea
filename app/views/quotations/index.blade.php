@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="well">
        {{Form::open(['route'=>'quotations.store'])}}
            <div class="form-group">
                {{Form::label('stock_material', 'Stock Material')}}
                {{Form::select('stock_material_id', StockMaterial::lists('name', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('stock_material_id')}}
            </div>
            <div class="form-group">
                {{Form::label('vendor', 'Vendor')}}
                {{Form::select('vendor_id', Vendor::lists('name', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('vendor_id')}}
            </div>
            <div class="form-group">
                {{Form::label('quantity', 'Quantity')}}
                {{Form::text('quantity', null, ['class'=>'form-control'])}}
                {{$errors->first('quantity')}}
            </div>
            <div class="form-group">
                {{Form::label('unit_price', 'Unit Price')}}
                {{Form::text('unit_price', null, ['class'=>'form-control'])}}
                {{$errors->first('unit_price')}}
            </div>
            <div class="form-group">
                {{Form::label('price_quote_statuss', 'Price Quote Status')}}
                <br/>
                {{Form::radio('price_quote_status', 1)}}
                <span>Yes</span>
                {{Form::radio('price_quote_status', 0, true)}}
                <span>No </span>
                {{$errors->first('price_quote_status')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>

    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Stock Material</th>
                    <th>Vendor</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Price</th>
                    <th>Price Quote Status</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($quotations as $quotation)
                    
                    <tr>
                        <td>{{@$quotation->stockMaterial->name}}</td>
                        <td>{{@$quotation->vendor->name}}</td>
                        <td>{{$quotation->quantity}}</td>
                        <td>{{$quotation->unit_price}}</td>
                        <td>{{$quotation->quantity*$quotation->unit_price}}</td>
                        <td>{{$quotation->price_quote_status}}</td>
                        <td style="text-align:center">
                            <a href="{{url('quotations/'.$quotation->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('quotations/destroy/'.$quotation->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop