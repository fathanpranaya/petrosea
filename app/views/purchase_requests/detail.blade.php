@extends('layouts.master')

@section('script')

<script>
    $('#datepick').datetimepicker({
        pickTime: false
    });
</script>

@stop

@section('content')
    <div class="well">

        <div class="row">


            <div class="col-lg-2"></div>
            
            <div class="col-lg-8">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Purchase request - {{$purchase_request->id}}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-brequested">
                            <tr>
                                <td>Subject</td>
                                <td>{{$purchase_request->pr_subject}}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>{{$purchase_request->pr_date}}</td>
                            </tr>
                            <tr>
                                <td>Required Date</td>
                                <td>{{$purchase_request->required_date}}</td>
                            </tr>
                            <tr>
                                <td>Requester</td>
                                <td>{{@$purchase_request->requester->name}}</td>
                            </tr>
                            <tr>
                                <td>Stock Material</td>
                                <td>{{@$purchase_request->stockMaterial->name}}</td>
                            </tr>
                            <tr>
                                <td>Purchase Order</td>
                                <td>{{@$purchase_request->po->po_subject}}</td>
                            </tr>
                            <tr>
                                <td>Quantity Material</td>
                                <td>{{$purchase_request->quantity_material.' '.@$purchase_request->uom->description}}</td>
                            </tr>
                            <tr>
                                <td>Unit Price</td>
                                <td>{{@$purchase_request->currency->name.' '.$purchase_request->unit_price}}</td>
                            </tr>
                            <tr>
                                <td>Total Price</td>
                                <td><b>{{@$purchase_request->currency->name.' '.$purchase_request->unit_price*$purchase_request->quantity_material}}</b></td>
                            </tr>
                            <tr>
                                <td>Person in Charge</td>
                                <td>{{$purchase_request->person_in_charge}}</td>
                            </tr>                            
                        </table>
                    </div><!-- /.box-body -->
                    
                </div><!-- /.box -->
            </div>
             
        </div>

    </div>
@stop