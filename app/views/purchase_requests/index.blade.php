@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
    <a href="{{url('purchase_requests/create')}}" class="btn btn-primary form-control"><span class="glyphicon glyphicon-plus-sign"></span> New Purchase Request</a>
    <hr>
    
    <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Date</th>
                <th>PIC</th>
                <th>Requester</th>
                <th>Stock Material</th>
                <th>Purchase Order</th>
                <th>Menu</th>
            </tr>
        </thead>
 
 
        <tbody>
            @foreach ($purchase_requests as $purchase_request)
                <tr>
                    <td>
                        <a href="{{url('purchase_requests/'.$purchase_request->id)}}">
                            {{$purchase_request->pr_subject}}
                        </a>
                    </td>        
                    <td>{{$purchase_request->pr_date}}</td>
                    <td>{{$purchase_request->person_in_charge}}</td>
                    <td>{{@$purchase_request->requester->name}}</td>
                    <td>{{@$purchase_request->stockMaterial->name}}</td>
                    <td>{{@$purchase_request->po->po_subject}}</td>
                    <td style="text-align:center">
                        <a href="{{url('purchase_requests/'.$purchase_request->id.'/edit')}}" class="btn btn-sm btn-success">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a> 
                        <a href="{{url('purchase_requests/destroy/'.$purchase_request->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
            @endforeach            
        </tbody>
    </table> </div>
    
@stop