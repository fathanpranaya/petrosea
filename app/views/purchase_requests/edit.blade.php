@extends('layouts.master')

@section('script')


@stop

@section('content')
    <div class="well">

        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-5">
                <h1 style="text-allign:center">Edit Purchase Request</h1>
            </div>
        </div>
        </hr>

        <div class="row">
           {{Form::model($purchase_request, ['route'=>['purchase_requests.update', $purchase_request->id], 'method' => 'PUT'])}}

            <div class="col-lg-2"></div>
            
            <div class="col-lg-4">
                <div class="form-group">
                    {{Form::label('requester', 'Requester')}}
                    {{Form::select('requester_id', Requester::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'requester_id'])}}
                    {{$errors->first('requester_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('stock_material', 'Stock Material')}}
                    {{Form::select('stock_material_id', StockMaterial::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'stock_material_id'])}}
                    {{$errors->first('stock_material_id')}}
                </div>

                <div class="form-group">
                    {{Form::label('po', 'Purchase Order')}}
                    {{Form::select('po_id', PurchaseOrder::lists('po_subject', 'id'), null, ['class'=>'form-control', 'id'=>'po_id'])}}
                    {{$errors->first('po_id')}}
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('quantity_material', 'Quantity')}}
                            {{Form::text('quantity_material', null, ['class'=>'form-control', 'id'=>'qt'])}}
                            {{$errors->first('quantity_material')}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('uom', 'Unit of Measure')}}
                            {{Form::select('uom_id', Uom::lists('description', 'id'), null, ['class'=>'form-control', 'id'=>'uom_id'])}}
                            {{$errors->first('uom_id')}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('unit_price', 'Unit Price')}}
                            {{Form::text('unit_price', null, ['class'=>'form-control', 'id'=>'price'])}}
                            {{$errors->first('unit_price')}}
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            {{Form::label('currency', 'Currency')}}
                            {{Form::select('currency_id', Currency::lists('name', 'id'), null, ['class'=>'form-control', 'id'=>'currency_id'])}}
                            {{$errors->first('currency_id')}}
                        </div>
                    </div>
                </div>                
            </div>
            
            <div class="col-lg-4">

                <div class="form-group">
                    {{Form::label('pr_subject', 'Subject')}}
                    {{Form::text('pr_subject', null, ['class'=>'form-control'])}}
                    {{$errors->first('pr_subject')}}
                </div>

                <div class="form-group">
                    {{Form::label('pr_date', 'Date')}}
                    <div class="input-group date" id="datepick1" data-date-format="YYYY-MM-DD">
                        {{Form::text('pr_date', null, array('class'=>'form-control '))}}    
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    {{$errors->first('pr_date')}}
                </div>
                

                <div class="form-group">                    
                    {{Form::label('required_date', 'Required Date')}}
                    <div class="input-group date" id="datepick2" data-date-format="YYYY-MM-DD">
                        {{Form::text('required_date', null, array('class'=>'form-control '))}}    
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    {{$errors->first('required_date')}}
                </div>


                <div class="form-group">
                    {{Form::label('person_in_charge', 'Person in Charge')}}
                    {{Form::text('person_in_charge', null, ['class'=>'form-control'])}}
                    {{$errors->first('person_in_charge')}}
                </div>

                <div class="form-group">
                    {{Form::submit('Save', ['class'=>'btn btn-primary', 'style' => 'width:100%'])}}
                </div>
            </div>

            {{Form::close()}}
                
        </div>

    </div>
@stop