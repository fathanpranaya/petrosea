@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
<h3>Edit Pick Ticket</h3>
<div class="row">
    <div class="col-lg-4">
        <div class="well">
        {{Form::model($pick_ticket, ['route'=>['pick_tickets.update', $pick_ticket->id], 'method' => 'PUT'])}}
            <div class="form-group">
                {{Form::label('stock_material', 'Stock Material')}}
                {{Form::select('stock_material_id', StockMaterial::lists('name', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('stock_material_id')}}
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {{Form::label('quantity', 'Quantity')}}
                        {{Form::text('quantity', null, ['class'=>'form-control', 'rows' => 2])}}
                        {{$errors->first('quantity')}}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {{Form::label('uom', 'Unit of Measure')}}
                        {{Form::select('uom_id', Uom::lists('name', 'id'), null, ['class'=>'form-control'])}}
                        {{$errors->first('uom_id')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('request_date', 'Request Date')}}
                <div class="input-group date" id="datepick1" data-date-format="YYYY-MM-DD">
                    {{Form::text('request_date', null, array('class'=>'form-control '))}}    
                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                </div>
                {{$errors->first('request_date')}}
            </div>
            <div class="form-group">
                {{Form::label('issue_date', 'Issue Date')}}
                <div class="input-group date" id="datepick2" data-date-format="YYYY-MM-DD">
                    {{Form::text('issue_date', null, array('class'=>'form-control '))}}    
                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                </div>
                {{$errors->first('issue_date')}}
            </div>
            <div class="form-group">
                {{Form::label('person_in_charge', 'Person in Charge')}}
                {{Form::text('person_in_charge', null, ['class'=>'form-control', 'rows' => 2])}}
                {{$errors->first('person_in_charge')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>

    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Stock Material</th>
                    <th>Unit of Measure</th>
                    <th>Quantity</th>
                    <th>Request Date</th>
                    <th>Issue Date</th>
                    <th>Person In Charge</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($pick_tickets as $pick_ticket)
                    
                    <tr>
                        <td>{{@$pick_ticket->stockMaterial->name}}</td>
                        <td>{{@$pick_ticket->uom->name}}</td>
                        <td>{{$pick_ticket->quantity}}</td>
                        <td>{{$pick_ticket->request_date}}</td>
                        <td>{{$pick_ticket->issue_date}}</td>
                        <td>{{$pick_ticket->person_in_charge}}</td>
                        <td style="text-align:center">
                            <a href="{{url('pick_tickets/'.$pick_ticket->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('pick_tickets/destroy/'.$pick_ticket->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop