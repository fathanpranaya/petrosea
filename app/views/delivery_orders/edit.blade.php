@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

    <script type="text/javascript">
        $(function() {
        $('#datetimepicker4').datetimepicker({
          pickTime: false
        });
        });
    </script>

@stop

@section('content')
<h3>Edit Delivery Order</h3>
<div class="row">
    <div class="col-lg-4">
        <div class="well">
         {{Form::model($delivery_order, ['route'=>['delivery_orders.update', $delivery_order->id], 'method' => 'PUT'])}}
            <div class="form-group">
                {{Form::label('purchase_order', 'Purchase Order')}}
                {{Form::select('purchase_order_id', PurchaseOrder::lists('po_subject', 'id'), null, ['class'=>'form-control'])}}
                {{$errors->first('purchase_order_id')}}
            </div>
            <div class="form-group">
                {{Form::label('date', 'Date')}}
                <div class="input-group date" id="datepick1" data-date-format="YYYY-MM-DD">
                    {{Form::text('date', null, array('class'=>'form-control '))}}    
                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                </div>
                {{$errors->first('date')}}
            </div>
            <div class="form-group">
                {{Form::label('descriptions', 'Description')}}
                {{Form::textarea('description', null, ['class'=>'form-control'])}}
                {{$errors->first('description')}}
            </div>
            <div class="form-group">
                {{Form::label('confirmance_statuss', 'Conformance Status')}}
                <br/>
                {{Form::radio('confirmance_status', 1)}}
                <span>Yes</span>
                {{Form::radio('confirmance_status', 0, true)}}
                <span>No</span>
                {{$errors->first('confirmance_status')}}
            </div>
            <div class="form-group">
                {{Form::label('delivery_statuss', 'Delivery Status')}}
                <br/>
                {{Form::radio('delivery_status', 1)}}
                <span>Yes</span>
                {{Form::radio('delivery_status', 0, true)}}
                <span>No</span>
                {{$errors->first('delivery_status')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>

    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Purchase Order</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Conformance Status</th>
                    <th>Delivery Status</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($delivery_orders as $delivery_order)
                    
                    <tr>
                        <td >{{@$delivery_order->purchaseOrder->po_subject}}</td>
                        <td >{{$delivery_order->date}}</td>
                        <td >{{$delivery_order->description}}</td>
                        <td >{{$delivery_order->confirmance_status}}</td>
                        <td >{{$delivery_order->delivery_status}}</td>
                        <td style="text-align:center">
                            <a href="{{url('delivery_orders/'.$delivery_order->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('delivery_orders/destroy/'.$delivery_order->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop