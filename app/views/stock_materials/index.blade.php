@extends('layouts.master')

@section('script')

    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.css')}}">
    <script src="{{asset('js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.js')}}"></script>

    <script>

        $(document).ready(function() {
            $('#table').dataTable({
               "scrollX": true
            });
        });

    </script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-4">
        <div class="well">
        {{Form::open(['route'=>'stock_materials.store'])}}
            <div class="form-group">
                {{Form::label('name', 'Name')}}
                {{Form::text('name', null, ['class'=>'form-control'])}}
                {{$errors->first('name')}}
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {{Form::label('quantity', 'Quantity')}}
                        {{Form::text('quantity', null, ['class'=>'form-control'])}}
                        {{$errors->first('quantity')}}
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group">
                        {{Form::label('uom', 'Unit of Measure')}}
                        {{Form::select('uom_id', Uom::lists('description', 'id'), null, ['class'=>'form-control'])}}
                        {{$errors->first('uom_id')}}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('description', 'Description')}}
                {{Form::textarea('description', null, ['class'=>'form-control', 'rows' => 4])}}
                {{$errors->first('description')}}
            </div>
            <div class="form-group">
                {{Form::submit('Add', ['class'=>'btn btn-primary', 'style'=>'width:100%'])}}
            </div>
        {{Form::close()}}
        </div>
    </div>
    <div class="col-lg-8">
        <div class="well"> <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Description</th>
                    <th>Menu</th>
                </tr>
            </thead>
     
     
            <tbody>
                @foreach ($stock_materials as $stock_material)
                    
                    <tr>
                        <td >{{$stock_material->name}}</td>
                        <td >{{$stock_material->quantity.' '.@$stock_material->uom->name}}</td>
                        <td >{{$stock_material->description}}</td>
                        <td style="text-align:center">
                            <a href="{{url('stock_materials/'.$stock_material->id.'/edit')}}" class="btn btn-sm btn-success">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a> 
                            <a href="{{url('stock_materials/destroy/'.$stock_material->id)}}" class="btn btn-sm btn-primary" onClick="return confirm('Are you sure you want to delete?')">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach            
            </tbody>
        </table> </div>
    </div>
</div>    
@stop