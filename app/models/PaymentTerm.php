<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PaymentTerm extends GenericModel {
	
	public $table = 'payment_terms';

	public $fillable = [
		'name',
		'description',
	];

	public $rules = [
		'name' => 'required|unique:payment_terms',
		'description' => 'required',
	];

}