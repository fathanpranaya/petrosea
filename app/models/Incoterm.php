<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Incoterm extends GenericModel {
	
	public $table = 'incoterms';

	public $fillable = [
		'name',
		'description',
	];

	public $rules = [
		'name' => 'required|unique:incoterms',
		'description' => 'required',
	];

}