<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AssignedProject extends GenericModel {
	
	public $table = 'assigned_projects';

	public $fillable = [
		'stock_material_id',
		'project_id',
	];

	public $rules = [
		'stock_material_id' => 'required',
		'project_id' => 'required',
	];

	public function stockMaterial()
	{
		return $this->belongsTo('StockMaterial', 'stock_material_id');
	}

	public function project()
	{
		return $this->belongsTo('project', 'project_id');
	}
	
}