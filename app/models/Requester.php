<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Requester extends GenericModel {
	
	public $table = 'requesters';

	public $fillable = [
		'name',
		'position',
		'long_work',
	];

	public $rules = [
		'name' => 'required',
		'position' => 'required',
		'long_work' => 'required|integer',
	];

}