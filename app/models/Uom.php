<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Uom extends GenericModel {
	
	public $table = 'uoms';

	public $fillable = [
		'name',
		'description',
	];

	public $rules = [
		'name' => 'required',
		'description' => 'required',
	];

}