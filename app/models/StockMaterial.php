<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class StockMaterial extends GenericModel {
	
	public $table = 'stock_materials';

	public $fillable = [
		'uom_id',
		'name',
		'quantity',
		'description',
	];

	public $rules = [
		'uom_id' => 'required',
		'name' => 'required',
		'quantity' => 'required|integer',
		'description' => '',
	];

	public function assigned_project()
	{
		return $this->belongsTo('AssignedProject', 'assigned_project_id');
	}

	public function uom()
	{
		return $this->belongsTo('Uom', 'uom_id');
	}

}