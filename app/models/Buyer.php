<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Buyer extends GenericModel {
	
	public $table = 'buyers';

	public $fillable = [
		'name',
		'position',
		'long_work',
	];

	public $rules = [
		'name' => 'required',
		'position' => 'required',
		'long_work' => 'required|integer',
	];

}