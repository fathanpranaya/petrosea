<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class DeliveryOrd extends GenericModel {
	
	public $table = 'delivery_orders';

	public $fillable = [
		'purchase_order_id',
		'date',
		'description',
		'confirmance_status',
		'delivery_status',
	];

	public $rules = [
		'purchase_order_id' => 'required',
		'date' => 'required|date',
		'description' => '',
		'confirmance_status' => 'required|integer',
		'delivery_status' => 'required|integer',
	];

	public function purchaseOrder()
	{
		return $this->belongsTo('PurchaseOrder', 'purchase_order_id');
	}


}