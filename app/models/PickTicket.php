<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PickTicket extends GenericModel {
	
	public $table = 'pick_tickets';

	public $fillable = [
		'stock_material_id',
		'uom_id',
		'quantity',
		'request_date',
		'issue_date',
		'person_in_charge',
	];

	public $rules = [
		'stock_material_id' => 'required',
		'uom_id' => 'required',
		'quantity' => 'required|integer',
		'request_date' => 'required|date',
		'issue_date' => 'required|date',
		'person_in_charge' => 'required',
	];

	public function stockMaterial()
	{
		return $this->belongsTo('StockMaterial', 'stock_material_id');
	}

	public function uom()
	{
		return $this->belongsTo('uom', 'uom_id');
	}
	
}