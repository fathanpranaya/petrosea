<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Project extends GenericModel {
	
	public $table = 'projects';

	public $fillable = [
		'name',
		'location',
		'duration',
		'status',
		'description',
	];

	public $rules = [
		'name' => 'required',
		'location' => 'required',
		'duration' => 'required|integer',
		'status' => 'required|integer',
		'description' => '',
	];

}