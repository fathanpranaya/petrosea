<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PurchaseRequest extends GenericModel {
	
	public $table = 'purchase_requests';

	public $fillable = [
		'requester_id',
		'stock_material_id',
		'po_id',
		'uom_id',
		'currency_id',
		'pr_subject',
		'pr_date',
		'quantity_material',
		'required_date',
		'unit_price',
		'person_in_charge',
	];

	public $rules = [
		'requester_id' => 'required',
		'stock_material_id' => 'required',
		'po_id' => 'required',
		'uom_id' => 'required',
		'currency_id' => 'required',
		'pr_subject' => 'required',
		'pr_date' => 'required|date',
		'quantity_material' => 'required|integer',
		'required_date' => 'required|date',
		'unit_price' => 'required|integer',
		'person_in_charge' => 'required',
	];

	public function requester()
	{
		return $this->belongsTo('Requester', 'requester_id');
	}

	public function stockMaterial()
	{
		return $this->belongsTo('StockMaterial', 'stock_material_id');
	}

	public function po()
	{
		return $this->belongsTo('PurchaseOrder', 'po_id');
	}

	public function uom()
	{
		return $this->belongsTo('Uom', 'uom_id');
	}

	public function currency()
	{
		return $this->belongsTo('Currency', 'currency_id');
	}
	
}