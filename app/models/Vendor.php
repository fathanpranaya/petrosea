<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Vendor extends GenericModel {
	
	public $table = 'vendors';

	public $fillable = [
		'name',
		'area_of_expertise',
		'contract_status',
		'performance_score',
	];

	public $rules = [
		'name' => 'required',
		'area_of_expertise' => 'required',
		'contract_status' => 'required|integer',
		'performance_score' => 'required|integer',
	];

}