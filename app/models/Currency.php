<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Currency extends GenericModel {
	
	public $table = 'currencies';

	public $fillable = [
		'name',
		'description',
	];

	public $rules = [
		'name' => 'required|unique:currencies',
		'description' => 'required',
	];

}