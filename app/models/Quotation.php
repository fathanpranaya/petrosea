<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Quotation extends GenericModel {
	
	public $table = 'quotations';

	public $fillable = [
		'stock_material_id',
		'vendor_id',
		'quantity',
		'unit_price',
		'price_quote_status',
	];

	public $rules = [
		'stock_material_id' => 'required',
		'vendor_id' => 'required',
		'quantity' => 'required|integer',
		'unit_price' => 'required|integer',
		'price_quote_status' => 'required|integer',
	];

	public function stockMaterial()
	{
		return $this->belongsTo('StockMaterial', 'stock_material_id');
	}

	public function vendor()
	{
		return $this->belongsTo('Vendor', 'vendor_id');
	}
	
}