<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PurchaseOrder extends GenericModel {
	
	public $table = 'purchase_orders';

	public $fillable = [
		'stock_material_id',
		'vendor_id',
		'buyer_id',
		'uom_id',
		'currency_id',
		'incoterm_id',
		'payment_term_id',
		'po_subject',
		'po_date',
		'quantity_material',
		'delivery_date',
		'unit_price',
		'delivery_address',
		'approval_status'
	];

	public $rules = [
		'stock_material_id' => 'required',
		'vendor_id' => 'required',
		'buyer_id' => 'required',
		'uom_id' => 'required',
		'currency_id' => 'required',
		'incoterm_id' => 'required',
		'payment_term_id' => 'required',
		'po_subject' => 'required',
		'po_date' => 'required|date',
		'quantity_material' => 'required',
		'delivery_date' => 'required|date',
		'unit_price' => 'required|integer',
		'delivery_address' => 'required',
		'approval_status' => 'required|integer',
	];

	public function stockMaterial()
	{
		return $this->belongsTo('StockMaterial', 'stock_material_id');
	}

	public function vendor()
	{
		return $this->belongsTo('Vendor', 'vendor_id');
	}

	public function buyer()
	{
		return $this->belongsTo('Buyer', 'buyer_id');
	}

	public function uom()
	{
		return $this->belongsTo('Uom', 'uom_id');
	}

	public function currency()
	{
		return $this->belongsTo('Currency', 'currency_id');
	}

	public function incoterm()
	{
		return $this->belongsTo('Incoterm', 'incoterm_id');
	}

	public function paymentTerm()
	{
		return $this->belongsTo('PaymentTerm', 'payment_term_id');
	}
	
}