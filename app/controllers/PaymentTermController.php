<?php

class PaymentTermController extends GenericController {

	public $names = 'payment_terms';
	public $name = 'payment_term';

	public function __construct(PaymentTerm $payment_term)
	{
		$this->db = $payment_term;
	}

}