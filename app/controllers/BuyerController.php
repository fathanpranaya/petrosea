<?php

class BuyerController extends GenericController {

	public $names = 'buyers';
	public $name = 'buyer';

	public function __construct(Buyer $buyer)
	{
		$this->db = $buyer;
	}

}