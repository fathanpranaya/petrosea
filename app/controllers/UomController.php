<?php

class UomController extends GenericController {

	public $names = 'uoms';
	public $name = 'uom';

	public function __construct(Uom $uom)
	{
		$this->db = $uom;
	}

}