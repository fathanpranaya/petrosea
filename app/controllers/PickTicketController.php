<?php

class PickTicketController extends GenericController {

	public $names = 'pick_tickets';
	public $name = 'pick_ticket';

	public function __construct(PickTicket $pick_ticket)
	{
		$this->db = $pick_ticket;
	}

}