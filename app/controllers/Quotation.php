<?php

class QuotationController extends GenericController {

	public $names = 'quotations';
	public $name = 'quotation';

	public function __construct(Quotation $quotation)
	{
		$this->db = $quotation;
	}

}