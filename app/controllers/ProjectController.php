<?php

class ProjectController extends GenericController {

	public $names = 'projects';
	public $name = 'project';

	public function __construct(Project $project)
	{
		$this->db = $project;
	}

}