<?php

class StockMaterialController extends GenericController {

	public $names = 'stock_materials';
	public $name = 'stock_material';

	public function __construct(StockMaterial $stock_material)
	{
		$this->db = $stock_material;
	}

}