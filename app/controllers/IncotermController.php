<?php

class IncotermController extends GenericController {

	public $names = 'incoterms';
	public $name = 'incoterm';

	public function __construct(Incoterm $incoterm)
	{
		$this->db = $incoterm;
	}

}