<?php

class PurchaseRequestController extends GenericController {

	public $names = 'purchase_requests';
	public $name = 'purchase_request';

	public function __construct(PurchaseRequest $purchase_request)
	{
		$this->db = $purchase_request;
	}

}