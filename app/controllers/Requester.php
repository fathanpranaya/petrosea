<?php

class RequesterController extends GenericController {

	public $names = 'requesters';
	public $name = 'requester';

	public function __construct(Requester $requester)
	{
		$this->db = $requester;
	}

}