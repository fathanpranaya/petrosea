<?php

class AssignedProjectController extends GenericController {

	public $names = 'assigned_projects';
	public $name = 'assigned_project';

	public function __construct(AssignedProject $assigned_project)
	{
		$this->db = $assigned_project;
	}

}