<?php

class DeliveryOrderController extends GenericController {

	public $names = 'delivery_orders';
	public $name = 'delivery_order';

	public function __construct(DeliveryOrd $delivery_order)
	{
		$this->db = $delivery_order;
	}

}