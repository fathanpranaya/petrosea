<?php

class PurchaseOrderController extends GenericController {

	public $names = 'purchase_orders';
	public $name = 'purchase_order';

	public function __construct(PurchaseOrder $purchase_order)
	{
		$this->db = $purchase_order;
	}

}