<?php

class CurrencyController extends GenericController {

	public $names = 'currencies';
	public $name = 'currency';

	public function __construct(Currency $currency)
	{
		$this->db = $currency;
	}

}