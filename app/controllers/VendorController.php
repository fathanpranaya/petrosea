<?php

class VendorController extends GenericController {

	public $names = 'vendors';
	public $name = 'vendor';

	public function __construct(Vendor $vendor)
	{
		$this->db = $vendor;
	}

}