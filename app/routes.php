<?php

Route::get('/', function() {
	return Redirect::to('admin');
});

Route::get('createuser', function() {
	User::truncate();
	User::create([
		'username' => 'admin',
		'password' => Hash::make('admin1234'),
		'email' => 'admin@petrosea.com',
		'role' => 'admin',
	]);
	User::create([
		'username' => 'inventory',
		'password' => Hash::make('inventory1234'),
		'email' => 'inventory@petrosea.com',
		'role' => 'inventory',
	]);
	User::create([
		'username' => 'procurement',
		'password' => Hash::make('procurement1234'),
		'email' => 'procurement@petrosea.com',
		'role' => 'procurement',
	]);
	User::create([
		'username' => 'warehouse',
		'password' => Hash::make('warehouse1234'),
		'email' => 'warehouse@petrosea.com',
		'role' => 'warehouse',
	]);
	User::create([
		'username' => 'external',
		'password' => Hash::make('external1234'),
		'email' => 'external@petrosea.com',
		'role' => 'external',
	]);
	return 'OK!';
});

Route::get('login', 'SessionController@create');
Route::get('logout', 'SessionController@destroy');

Route::resource('sessions', 'SessionController');
Route::resource('users', 'UserController');

// Route::get('search/{q}', 'HomeController@search');


Route::group(['before' => 'auth'], function() {

	Route::get('admin', function ()
	{
		return View::make('hello');	
	});

// 	Route::get('salespersons/all', 'SalespersonController@all');
// 	Route::get('salespersons/destroy/{id}', 'SalespersonController@destroy');
// 	Route::resource('salespersons', 'SalespersonController');
	
// 	Route::get('sales/destroy/{id}', 'SaleController@destroy');
// 	Route::put('sales/directupdate/{id}', 'SaleController@directupdate');
// 	Route::get('lead', 'SaleController@lead');
// 	Route::get('prospect', 'SaleController@prospect');
// 	Route::get('deal', 'SaleController@deal');
// 	Route::resource('sales', 'SaleController');

// 	Route::get('companies/all', 'CompanyController@all');
// 	Route::get('companies/destroy/{id}', 'CompanyController@destroy');
// 	Route::resource('companies', 'CompanyController');

// 	Route::get('contacts/all', 'ContactController@all');
// 	Route::get('contacts/destroy/{id}', 'ContactController@destroy');
// 	Route::resource('contacts', 'ContactController');


	Route::get('pick_tickets/destroy/{id}', 'PickTicketController@destroy');
	Route::resource('pick_tickets', 'PickTicketController');

	Route::get('delivery_orders/destroy/{id}', 'DeliveryOrderController@destroy');
	Route::resource('delivery_orders', 'DeliveryOrderController');

	Route::get('purchase_orders/destroy/{id}', 'PurchaseOrderController@destroy');
	Route::resource('purchase_orders', 'PurchaseOrderController');

	Route::get('vendors/destroy/{id}', 'VendorController@destroy');
	Route::resource('vendors', 'VendorController');

	Route::get('projects/destroy/{id}', 'ProjectController@destroy');
	Route::resource('projects', 'ProjectController');

	Route::get('assigned_projects/destroy/{id}', 'AssignedProjectController@destroy');
	Route::resource('assigned_projects', 'AssignedProjectController');

	Route::get('currencies/destroy/{id}', 'CurrencyController@destroy');
	Route::resource('currencies', 'CurrencyController');

	Route::get('uoms/destroy/{id}', 'UomController@destroy');
	Route::resource('uoms', 'UomController');

	Route::get('payment_terms/destroy/{id}', 'PaymentTermController@destroy');
	Route::resource('payment_terms', 'PaymentTermController');

	Route::get('incoterms/destroy/{id}', 'IncotermController@destroy');
	Route::resource('incoterms', 'IncotermController');

	Route::get('buyers/destroy/{id}', 'BuyerController@destroy');
	Route::resource('buyers', 'BuyerController');

	Route::get('requesters/destroy/{id}', 'RequesterController@destroy');
	Route::resource('requesters', 'RequesterController');

	Route::get('purchase_requests/destroy/{id}', 'PurchaseRequestController@destroy');
	Route::resource('purchase_requests', 'PurchaseRequestController');

	Route::get('quotations/destroy/{id}', 'QuotationController@destroy');
	Route::resource('quotations', 'QuotationController');

	Route::get('stock_materials/destroy/{id}', 'StockMaterialController@destroy');
	Route::resource('stock_materials', 'StockMaterialController');


// 	Route::get('documents/destroy/{id}', 'DocumentController@destroy');
// 	Route::resource('documents', 'DocumentController');

// 	Route::get('sales_comments/destroy/{id}', 'SalesCommentController@destroy');
// 	Route::resource('sales_comments', 'SalesCommentController');

// 	Route::get('user/edit', 'UserController@useredit');
// 	Route::post('user/update', 'UserController@userupdate');

// 	Route::get('user/profile', 'SalespersonController@profile');
});

// Route::group(['before' => 'manager'], function() {

// 	Route::controller('reports', 'ReportController');

// });

// Route::group(['before' => 'admin'], function() {
// 	Route::get('task_categories/destroy/{id}', 'TaskCategoryController@destroy');
// 	Route::resource('task_categories', 'TaskCategoryController');

// 	Route::get('sales_priorities/destroy/{id}', 'SalesPriorityController@destroy');
// 	Route::resource('sales_priorities', 'SalesPriorityController');

// 	Route::get('sales_referrals/destroy/{id}', 'SalesReferralController@destroy');
// 	Route::resource('sales_referrals', 'SalesReferralController');

// 	Route::get('stages/destroy/{id}', 'StageController@destroy');
// 	Route::resource('stages', 'StageController');

// 	Route::get('stage_categories/destroy/{id}', 'StageCategoryController@destroy');
// 	Route::resource('stage_categories', 'StageCategoryController');

// 	Route::get('solutions/destroy/{id}', 'SolutionController@destroy');
// 	Route::resource('solutions', 'SolutionController');

// 	Route::get('solution_categories/destroy/{id}', 'SolutionCategoryController@destroy');
// 	Route::resource('solution_categories', 'SolutionCategoryController');

// 	Route::get('contact_roles/destroy/{id}', 'ContactRoleController@destroy');
// 	Route::resource('contact_roles', 'ContactRoleController');

// 	Route::get('users/destroy/{id}', 'UserController@destroy');
// 	Route::resource('users', 'UserController');

// 	Route::get('sales_stages/destroy/{id}', 'SalesStageController@destroy');
// 	Route::resource('sales_stages', 'SalesStageController');

// 	Route::get('salesperson_contacts/destroy/{id}', 'SalespersonContactController@destroy');
// 	Route::resource('salesperson_contacts', 'SalespersonContactController');
// });

HTML::macro('nav_link', function($route, $text) {
	$link = explode('/', Request::path());
	if ($link[0] == $route)
		$active = "class = 'active'";
	else
		$active = '';

  	return '<li '.$active.'><a href="'.url($route).'" ><i class="fa fa-angle-double-right"></i>'.$text.'</a></li>';
});

