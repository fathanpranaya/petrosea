# SuitCRM
Platform for customer relationship management system, developed by Suitmedia


![SuitCRM](http://i.imgur.com/TbVg0FM.png) 


### Main Components:
* Laravel 4.2
* barryvdh/laravel-migration-generator
* greggilbert/recaptcha
* jQuery v2.0.0
* Bootstrap v3.0.1
* DataTables 1.10.0
* Highcharts JS v4.0.1
* bootstrap-datetimepicker.js V3.0.0
* FullCalendar v2.0.0
* LESS - Leaner CSS v1.3.3
* moment.js v2.5.1


### Features:

1. Login and user management
2. Sales management
3. Company and Contact (PIC) management
4. Salesperson management and monitoring by "manager" user
5. Weekly report for "manager" user
6. Search company, contact and sales


### How to use in real project:
1. Copy all files from project **suitcrm**
2. Remove these folders: **.git**
3. Set up the database
4. Type this url ("*your_domain.com* **/createuser**") in browser. It will return 'OK'
5. Finally, back to homepage and login with username : **yogi**, password : **mangontang**

### How to setup the database:

1. Create a new database with name : **suitcrm**

2. Run this command in your suitcrm root folder

		$ php artisan migrate

### Changelog:

#### Version 0.1 (2014-06-25)

* First release

####Contact us
* Fathan : fathan.pranaya@gmail.com (+6285717573330)
* Sonny  : sonnylazuardi@gmail.com  (+622134005315)